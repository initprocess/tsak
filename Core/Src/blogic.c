/**
  ******************************************************************************
  * @file           : blogic.c
  * @brief          : Логика работы конкретного контроллера
  ******************************************************************************
  * Created on: 28 июл. 2020 г.
  * This software component is licensed by Nizovoi Grigorii under GPL v2+,
  * the "License";
  ******************************************************************************
  */
#include "blogic.h"
#include "modbus.h"

extern uint16_t modbus_holding_registers[];
extern uint16_t modbus_input_registers[];

void blogic()
{
}
