/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "adc.h"
#include "dma.h"
#include "i2c.h"
#include "spi.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "modbus.h"
#include "blogic.h"
#include <math.h>

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
/**
 * @bref Массив, куда DMA сливает результаты измерений
 * 0 - AN6
 * 1 - AN5
 * 2 - AN4
 * 3 - AN3
 * 4 - AN2
 * 5 - AN1
 * 6 - AN0
 * 7 - MAIN_I
 * 8 - DRV1_I
 * 9 - DRV2_I
 * 10 - CPU_TEMP
**/
volatile uint16_t adc[11];
extern uint16_t modbus_holding_registers[];
extern uint16_t modbus_input_registers[];
extern uint8_t rx_buff[];
extern uint16_t rx_buff_len;

volatile uint8_t hours = 0;
volatile uint8_t minutes = 0;
volatile uint16_t milliseconds = 0;

int adc_counter = 0; 	///< Счетчик измерений аналоговых сигналов
uint8_t eom = 0; 		///< Конец цикла измерений
uint32_t an0_avg, an1_avg, an2_avg, an3_avg, an4_avg, an5_avg, an6_avg, drv1_i_avg, drv2_i_avg, main_i_avg = 0; ///< Переменные для вычисления среднего значения сигналов
double drv1_i_raw, drv2_i_raw, main_i_raw = 0;
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
/**
 * @brief Прерывание ADC DMA transfer
 * @param hadc ADC handle structure
 * @retval нет
 */
void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* hadc)
{
    	/*
    	 * adc[]:
    	 * 0 - AN6
    	 * 1 - AN5
    	 * 2 - AN4
    	 * 3 - AN3
    	 * 4 - AN2
    	 * 5 - AN1
    	 * 6 - AN0
    	 * 7 - MAIN_I
    	 * 8 - DRV1_I
    	 * 9 - DRV2_I
    	 * 10 - CPU_TEMP
    	**/
	if (eom == 0) {
		an0_avg += adc[6];
		an1_avg += adc[5];
		an2_avg += adc[4];
		an3_avg += adc[3];
		an4_avg += adc[2];
		an5_avg += adc[1];
		an6_avg += adc[0];
		main_i_avg += adc[7];
		drv1_i_avg += adc[8];
		drv2_i_avg += adc[9];
		adc_counter++;
		if (adc_counter >= ADC_MEAS_PERIOD) {
			adc_counter = 0;
			eom = 1;
		}
	}
//-------------------------------------------------------------------
}
/**
 * @brief Прерывание, если на шине RS485 кончилась передача. Конец передачи пакета MODBUS.
 * @param *huart UART handle structurе pointer
 * @retval нет
 */
void HAL_UART_IDLE_Callback(UART_HandleTypeDef *huart)
{
	if(huart == &huart1)
	{
		// rx_tx_act = 1; // Моргнуть светодиодом
		__HAL_UART_DISABLE_IT(&huart1, UART_IT_IDLE);
		rx_buff_len = BUFSIZE - __HAL_DMA_GET_COUNTER(huart->hdmarx);
		if (rx_buff[0] == MBADDR) {
			if ((uint16_t)((rx_buff[rx_buff_len-1] << 8) | rx_buff[rx_buff_len-2]) == CRC16(rx_buff, rx_buff_len-2)) Modbus_response();
			else Internal_error(MOD_ERROR_CRC);
		}
		HAL_UART_AbortReceive(&huart1);
		__HAL_UART_CLEAR_IDLEFLAG(&huart1);
		__HAL_UART_ENABLE_IT(&huart1, UART_IT_IDLE);
		HAL_UART_Receive_DMA(&huart1, (uint8_t*)rx_buff, BUFSIZE);
	}
}
/**
 * @brief Прерывание, если буфер RS485 заполнен.
 * @param *huart UART handle structurе pointer
 * @retval нет
 */
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
	  if(huart == &huart1)
	  {
		  __HAL_UART_DISABLE_IT(&huart1, UART_IT_IDLE);
		  //HAL_UART_DMAStop(&huart1);
		  Internal_error(UART_ERROR_BUFFER);
		  HAL_UART_AbortReceive(&huart1);
		  __HAL_UART_CLEAR_IDLEFLAG(&huart1);
		  __HAL_UART_ENABLE_IT(&huart1, UART_IT_IDLE);
		  HAL_UART_Receive_DMA(&huart1, (uint8_t*)rx_buff, BUFSIZE);
	  }
}
/**
 * @brief Прерывание по ошибкам RS485 UART.
 * @param *huart UART handle structurе pointer
 * @retval нет
 */
void HAL_UART_ErrorCallback(UART_HandleTypeDef *huart)
{
	if(huart == &huart1)
	{
		//__HAL_UART_DISABLE_IT(&huart1, UART_IT_IDLE);
		HAL_UART_DMAStop(&huart1);
		// HAL_GPIO_WritePin(USART_DE_GPIO_Port, USART_DE_Pin, GPIO_PIN_RESET);
		uint32_t er = HAL_UART_GetError(&huart1);

		switch(er)
		{
			case HAL_UART_ERROR_PE:
				Internal_error(UART_ERROR_PE);
				__HAL_UART_CLEAR_PEFLAG(&huart1);
				huart->ErrorCode = HAL_UART_ERROR_NONE;
			break;
			case HAL_UART_ERROR_NE:
				Internal_error(UART_ERROR_NE);
				__HAL_UART_CLEAR_NEFLAG(&huart1);
				huart->ErrorCode = HAL_UART_ERROR_NONE;
			break;
			case HAL_UART_ERROR_FE:
				Internal_error(UART_ERROR_FE);
				__HAL_UART_CLEAR_FEFLAG(&huart1);
				huart->ErrorCode = HAL_UART_ERROR_NONE;
			break;
			case HAL_UART_ERROR_ORE:
				Internal_error(UART_ERROR_ORE);
				__HAL_UART_CLEAR_OREFLAG(huart);
				huart->ErrorCode = HAL_UART_ERROR_NONE;
			break;
			case HAL_UART_ERROR_DMA:
				Internal_error(UART_ERROR_DMA);
				huart->ErrorCode = HAL_UART_ERROR_NONE;
			break;
			default:
			break;
		}
	}
}
/**
 * @brief  Period elapsed callback in non blocking mode
 * @note   This function is called  when TIM17 interrupt took place, inside
 * HAL_TIM_IRQHandler(). It makes a direct call to HAL_IncTick() to increment
 * a global variable "uwTick" used as application time base.
 * @param  htim : TIM handle
 * @retval None
 */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
 /* USER CODE BEGIN Callback 0 */

 /* USER CODE END Callback 0 */
 if (htim->Instance == TIM17)
 {
   HAL_IncTick();
 }
 /* USER CODE BEGIN Callback 1 */
	if(htim->Instance == TIM16)
	{
//-----------------------clocks---------------------------------------------------
		if (modbus_holding_registers[MOD_H_SET] != 65535)
		{
			hours = modbus_holding_registers[MOD_H_SET];
			modbus_holding_registers[MOD_H_SET] = 65535;
		}
		if (modbus_holding_registers[MOD_M_SET] != 65535)
		{
			minutes = modbus_holding_registers[MOD_M_SET];
			modbus_holding_registers[MOD_M_SET] = 65535;
		}
		if (modbus_holding_registers[MOD_S_SET] != 65535)
		{
			milliseconds = modbus_holding_registers[MOD_S_SET]*1000;
			modbus_holding_registers[MOD_S_SET] = 65535;
		}
		milliseconds++;
		if (milliseconds == 60000)
		{
			milliseconds = 0;
			minutes++;
		}
		modbus_input_registers[MOD_MSEC] = milliseconds;
		if (minutes == 60)
		{
			minutes = 0;
			hours++;
		}
		modbus_input_registers[MOD_MINUTES] = minutes;
		if (hours == 24)
		{
			hours = 0;
		}
		modbus_input_registers[MOD_HOURS] = hours;
//--------------------------------------------------------------------------------
	}
 /* USER CODE END Callback 1 */
}
/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_ADC_Init();
  MX_I2C1_Init();
  MX_SPI1_Init();
  MX_USART1_UART_Init();
  MX_TIM3_Init();
  MX_TIM16_Init();
  /* USER CODE BEGIN 2 */
  modbus_input_registers[MOD_FIRMWARE_VER] = FIRMWARE_VERSION;
  HAL_ADCEx_Calibration_Start(&hadc);
  HAL_ADC_Start_DMA(&hadc, (uint32_t*)&adc, 11);
  HAL_TIM_Base_Start(&htim3);
  HAL_TIM_Base_Start_IT(&htim16);
  __HAL_UART_ENABLE_IT(&huart1, UART_IT_IDLE);
  HAL_UART_Receive_DMA(&huart1, (uint8_t*)rx_buff, BUFSIZE);
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  HAL_GPIO_WritePin(DRV1_F_GPIO_Port, DRV1_F_Pin,GPIO_PIN_RESET);
  HAL_GPIO_WritePin(DRV1_B_GPIO_Port, DRV1_B_Pin,GPIO_PIN_RESET);
  float temp;
  while (1)
  {
	  if (eom == 1) {
		  modbus_input_registers[MOD_AN0] = an0_avg/ADC_MEAS_PERIOD;
		  modbus_input_registers[MOD_AN1] = an1_avg/ADC_MEAS_PERIOD;
		  modbus_input_registers[MOD_AN2] = an2_avg/ADC_MEAS_PERIOD;
		  modbus_input_registers[MOD_AN3] = an3_avg/ADC_MEAS_PERIOD;
		  modbus_input_registers[MOD_AN4] = an4_avg/ADC_MEAS_PERIOD;
		  modbus_input_registers[MOD_AN5] = an5_avg/ADC_MEAS_PERIOD;
		  modbus_input_registers[MOD_AN6] = an6_avg/ADC_MEAS_PERIOD;
		  drv1_i_raw = (double)drv1_i_avg/ADC_MEAS_PERIOD;
		  drv2_i_raw = (double)drv2_i_avg/ADC_MEAS_PERIOD;
		  main_i_raw = (double)main_i_avg/ADC_MEAS_PERIOD;
		  an0_avg = 0; an1_avg = 0; an2_avg = 0; an3_avg = 0; an4_avg = 0; an5_avg = 0; an6_avg = 0;
		  drv1_i_avg = 0; drv2_i_avg = 0; main_i_avg = 0;
		  modbus_input_registers[MOD_DRV1_I] = (uint16_t)(drv1_i_raw*ADC_SLOPE/DRV1_I_COEFF*1000);
		  modbus_input_registers[MOD_DRV2_I] = (uint16_t)(drv2_i_raw*ADC_SLOPE/DRV2_I_COEFF*1000);
		  modbus_input_registers[MOD_MAIN_I] = (uint16_t)(main_i_raw*ADC_SLOPE/MAIN_I_COEFF*1000);
		  temp = VDD_AD/4096*adc[10];
		  temp = (CPU_T_V25-temp)/CPU_T_VSLOPE+25.0;
		  modbus_input_registers[MOD_THERMO] = temp;

		  eom = 0;
	  }
	  /*
	   * Внутренняя логика не отключена?
	   */
	  if (modbus_input_registers[MOD_CONTROL] & 1 == 0) {
		  blogic();
	  }

    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI|RCC_OSCILLATORTYPE_HSI14
                              |RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSI14State = RCC_HSI14_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.HSI14CalibrationValue = 16;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL6;
  RCC_OscInitStruct.PLL.PREDIV = RCC_PREDIV_DIV1;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_1) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USART1|RCC_PERIPHCLK_I2C1;
  PeriphClkInit.Usart1ClockSelection = RCC_USART1CLKSOURCE_PCLK1;
  PeriphClkInit.I2c1ClockSelection = RCC_I2C1CLKSOURCE_HSI;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  Period elapsed callback in non blocking mode
  * @note   This function is called  when TIM17 interrupt took place, inside
  * HAL_TIM_IRQHandler(). It makes a direct call to HAL_IncTick() to increment
  * a global variable "uwTick" used as application time base.
  * @param  htim : TIM handle
  * @retval None
  */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
  /* USER CODE BEGIN Callback 0 */

  /* USER CODE END Callback 0 */
  if (htim->Instance == TIM17) {
    HAL_IncTick();
  }
  /* USER CODE BEGIN Callback 1 */
	if(htim->Instance == TIM16)
	{
//-----------------------clocks---------------------------------------------------
		if (modbus_holding_registers[MOD_H_SET] != 65535)
		{
			hours = modbus_holding_registers[MOD_H_SET];
			modbus_holding_registers[MOD_H_SET] = 65535;
		}
		if (modbus_holding_registers[MOD_M_SET] != 65535)
		{
			minutes = modbus_holding_registers[MOD_M_SET];
			modbus_holding_registers[MOD_M_SET] = 65535;
		}
		if (modbus_holding_registers[MOD_S_SET] != 65535)
		{
			milliseconds = modbus_holding_registers[MOD_S_SET]*1000;
			modbus_holding_registers[MOD_S_SET] = 65535;
		}
		milliseconds++;
		if (milliseconds == 60000)
		{
			milliseconds = 0;
			minutes++;
		}
		modbus_input_registers[MOD_MSEC] = milliseconds;
		if (minutes == 60)
		{
			minutes = 0;
			hours++;
		}
		modbus_input_registers[MOD_MINUTES] = minutes;
		if (hours == 24)
		{
			hours = 0;
		}
		modbus_input_registers[MOD_HOURS] = hours;
//--------------------------------------------------------------------------------
	}
  /* USER CODE END Callback 1 */
}

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
