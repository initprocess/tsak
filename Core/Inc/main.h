/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f0xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define PM3_Pin GPIO_PIN_13
#define PM3_GPIO_Port GPIOC
#define PM4_Pin GPIO_PIN_14
#define PM4_GPIO_Port GPIOC
#define LED_ACT_Pin GPIO_PIN_15
#define LED_ACT_GPIO_Port GPIOC
#define AN6_Pin GPIO_PIN_0
#define AN6_GPIO_Port GPIOA
#define AN5_Pin GPIO_PIN_1
#define AN5_GPIO_Port GPIOA
#define AN4_Pin GPIO_PIN_2
#define AN4_GPIO_Port GPIOA
#define AN3_Pin GPIO_PIN_3
#define AN3_GPIO_Port GPIOA
#define AN2_Pin GPIO_PIN_4
#define AN2_GPIO_Port GPIOA
#define AN1_Pin GPIO_PIN_5
#define AN1_GPIO_Port GPIOA
#define AN0_Pin GPIO_PIN_6
#define AN0_GPIO_Port GPIOA
#define MAIN_I_Pin GPIO_PIN_7
#define MAIN_I_GPIO_Port GPIOA
#define DRV1_I_Pin GPIO_PIN_0
#define DRV1_I_GPIO_Port GPIOB
#define DRV2_I_Pin GPIO_PIN_1
#define DRV2_I_GPIO_Port GPIOB
#define GPIO7_Pin GPIO_PIN_2
#define GPIO7_GPIO_Port GPIOB
#define GPIO6_Pin GPIO_PIN_10
#define GPIO6_GPIO_Port GPIOB
#define GPIO5_Pin GPIO_PIN_11
#define GPIO5_GPIO_Port GPIOB
#define GPIO4_Pin GPIO_PIN_12
#define GPIO4_GPIO_Port GPIOB
#define GPIO3_Pin GPIO_PIN_13
#define GPIO3_GPIO_Port GPIOB
#define GPIO2_Pin GPIO_PIN_14
#define GPIO2_GPIO_Port GPIOB
#define GPIO1_Pin GPIO_PIN_15
#define GPIO1_GPIO_Port GPIOB
#define GPIO0_Pin GPIO_PIN_8
#define GPIO0_GPIO_Port GPIOA
#define USART_TX_Pin GPIO_PIN_9
#define USART_TX_GPIO_Port GPIOA
#define USART_RX_Pin GPIO_PIN_10
#define USART_RX_GPIO_Port GPIOA
#define DRV1_B_Pin GPIO_PIN_11
#define DRV1_B_GPIO_Port GPIOA
#define USART_DE_Pin GPIO_PIN_12
#define USART_DE_GPIO_Port GPIOA
#define DRV1_F_Pin GPIO_PIN_6
#define DRV1_F_GPIO_Port GPIOF
#define DRV2_B_Pin GPIO_PIN_7
#define DRV2_B_GPIO_Port GPIOF
#define DRV2_F_Pin GPIO_PIN_15
#define DRV2_F_GPIO_Port GPIOA
#define PM1_Pin GPIO_PIN_8
#define PM1_GPIO_Port GPIOB
#define PM2_Pin GPIO_PIN_9
#define PM2_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */
/**
 * Роль контроллера:
 * GREENHOUSE	теплица
 * TANK			контроллер бака
 */
#define GREENHOUSE
#define VDD_AD			3.569 		///< Напряжение аналоговой части
#define ADC_SLOPE 		VDD_AD/4096 ///< Напряжение одного разряда ЦАП
#define CPU_T_V25		1.41		///< Напряжение (в вольтах) на внутреннем датчике при температуре 25 °C.
#define CPU_T_VSLOPE	0.0043		///< Изменение напряжения (в вольтах) при изменении температуры на градус.

#define ADC_MEAS_PERIOD 50 			///< Период измерения ADC
#define MAIN_I_COEFF 	(0.008*50) 	///< MAIN I: R shunt / amp gain
#define DRV1_I_COEFF 	(0.008*50) 	///< DRV1 I: R shunt / amp gain
#define DRV2_I_COEFF 	(0.008*50) 	///< DRV2 I: R shunt / amp gain

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
