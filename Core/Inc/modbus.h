#ifndef __MODBUS_H
#define __MODBUS_H

#include "main.h"


#define FIRMWARE_VERSION	41		///< версия микропрограммы
#define BUFSIZE				1024	///< размер буфера приема RS485
#define MBADDR				10		///< MODBUS адрес устройства
#define MBINPREGS_MAIN		18		///< количество MODBUS INPUT регистров (только для чтения)
#define MBHOLDREGS_MAIN		11		///< количество MODBUS HOLDING регистров (чтение и запись)

/*
 * Modbus input registers
 */
#define MOD_FIRMWARE_VER 0	///< MODBUS input reg: версия микропрограммы
#define MOD_ERROR_COUNT	1	///< MODBUS input reg: счетчик ошибок
#define MOD_LAST_ERROR	2	///< MODBUS input reg: номер последней ошибки
#define MOD_HOURS		3	///< MODBUS input reg: часы
#define MOD_MINUTES		4	///< MODBUS input reg: минуты
#define MOD_MSEC	 	5	///< MODBUS input reg: миллисекунды
#define MOD_GPI 		6	///< MODBUS input reg: входы GPIO с бита 0 по 7
#define MOD_AN0 		7	///< MODBUS input reg: аналоговый вход 0 значение АЦП 0-4095
#define MOD_AN1 		8	///< MODBUS input reg: аналоговый вход 1 значение АЦП 0-4095
#define MOD_AN2 		9	///< MODBUS input reg: аналоговый вход 2 значение АЦП 0-4095
#define MOD_AN3 		10	///< MODBUS input reg: аналоговый вход 3 значение АЦП 0-4095
#define MOD_AN4 		11	///< MODBUS input reg: аналоговый вход 4 значение АЦП 0-4095
#define MOD_AN5 		12	///< MODBUS input reg: аналоговый вход 5 значение АЦП 0-4095
#define MOD_AN6 		13	///< MODBUS input reg: аналоговый вход 6 значение АЦП 0-4095
#define MOD_MAIN_I 		14	///< MODBUS input reg: общий силовой ток мА
#define MOD_DRV1_I 		15	///< MODBUS input reg: ток 1 двигателя мА
#define MOD_DRV2_I 		16	///< MODBUS input reg: ток 2 двигателя мА
#define MOD_THERMO 		17	///< MODBUS input reg: температура микроконтроллера C
/*
 * Modbus holding registers
 */
#define MOD_CONTROL 0	///< MODBUS holding reg: регистр контроля состояния 0:откл. внутр. логику
#define MOD_H_SET	1	///< MODBUS holding reg: установка часов
#define MOD_M_SET	2	///< MODBUS holding reg: установка минут
#define MOD_S_SET	3	///< MODBUS holding reg: установка секунд
#define MOD_GPO 	4	///< MODBUS holding reg: цифровые выходы с 0 по 7
#define MOD_PM1 	5	///< MODBUS holding reg: мощный выход 1 с ШИМ 0-99
#define MOD_PM2 	6	///< MODBUS holding reg: мощный выход 2 с ШИМ 0-99
#define MOD_PM3 	7	///< MODBUS holding reg: мощный выход 3 с ШИМ 0-99
#define MOD_PM4 	8	///< MODBUS holding reg: мощный выход 4 с ШИМ 0-99
#define MOD_DRV1 	9	///< MODBUS holding reg: первый двигатель 0 - назад 127 - выкл 255 - вперед
#define MOD_DRV2 	10	///< MODBUS holding reg: второй двигатель 0 - назад 127 - выкл 255 - вперед
//======================================================================================================
/**
 * Custom registers
 */

//---------------------------------------------------------------------------------------------------
/*
 * Internal errors
 */
#define UART_ERROR_PE 		1 	///< USART Parity error
#define UART_ERROR_NE 		2 	///< USART Noise error
#define UART_ERROR_FE 		3 	///< USART Frame error
#define UART_ERROR_ORE 		4 	///< USART Overrun error
#define UART_ERROR_DMA 		5 	///< USART DMA transfer error
#define UART_ERROR_BUFFER 	6 	///< USART buffer owerflow
#define UART_ERROR_HAL 		7 	///< USART HAL_ERROR - rx_buff == NULL or rx_buff_len == 0
#define MOD_ERROR_CRC 		8	///< MODBUS crc error

void Modbus_error(int err_num);
void Internal_error(uint16_t err_num);
uint8_t Modbus_do(uint16_t addr, uint16_t data);
void Modbus_response(void);
uint16_t CRC16 (uint8_t *nData, uint16_t wLength);

#endif /* __MODBUS_H */
