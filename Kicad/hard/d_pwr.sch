EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 8 8
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L User:MP2451-Regulator_Switching U?
U 1 1 5F6D955C
P 3775 2800
AR Path="/5F6D955C" Ref="U?"  Part="1" 
AR Path="/5F687A24/5F6D955C" Ref="U7"  Part="1" 
F 0 "U7" H 3750 2865 50  0000 C CNN
F 1 "MP2451" H 3750 2774 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23-6" H 3775 2800 50  0001 C CNN
F 3 "" H 3775 2800 50  0001 C CNN
	1    3775 2800
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5F6D9562
P 3225 2750
AR Path="/5F6D9562" Ref="C?"  Part="1" 
AR Path="/5F687A24/5F6D9562" Ref="C16"  Part="1" 
F 0 "C16" H 3317 2796 50  0000 L CNN
F 1 "100nF" H 3317 2705 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 3225 2750 50  0001 C CNN
F 3 "~" H 3225 2750 50  0001 C CNN
	1    3225 2750
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5F6D9568
P 3775 3600
AR Path="/5F6D9568" Ref="C?"  Part="1" 
AR Path="/5F687A24/5F6D9568" Ref="C17"  Part="1" 
F 0 "C17" H 3867 3646 50  0000 L CNN
F 1 "33pF" H 3867 3555 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 3775 3600 50  0001 C CNN
F 3 "~" H 3775 3600 50  0001 C CNN
	1    3775 3600
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R?
U 1 1 5F6D956E
P 3425 3600
AR Path="/5F6D956E" Ref="R?"  Part="1" 
AR Path="/5F687A24/5F6D956E" Ref="R40"  Part="1" 
F 0 "R40" H 3484 3646 50  0000 L CNN
F 1 "130K" H 3484 3555 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 3425 3600 50  0001 C CNN
F 3 "~" H 3425 3600 50  0001 C CNN
	1    3425 3600
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R?
U 1 1 5F6D9574
P 3075 3600
AR Path="/5F6D9574" Ref="R?"  Part="1" 
AR Path="/5F687A24/5F6D9574" Ref="R39"  Part="1" 
F 0 "R39" H 3134 3646 50  0000 L CNN
F 1 "37.4K" H 3134 3555 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 3075 3600 50  0001 C CNN
F 3 "~" H 3075 3600 50  0001 C CNN
	1    3075 3600
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F6D957A
P 3075 3700
AR Path="/5F6D957A" Ref="#PWR?"  Part="1" 
AR Path="/5F687A24/5F6D957A" Ref="#PWR061"  Part="1" 
F 0 "#PWR061" H 3075 3450 50  0001 C CNN
F 1 "GND" H 3080 3527 50  0000 C CNN
F 2 "" H 3075 3700 50  0001 C CNN
F 3 "" H 3075 3700 50  0001 C CNN
	1    3075 3700
	1    0    0    -1  
$EndComp
Wire Wire Line
	3075 3500 3425 3500
Wire Wire Line
	3425 3500 3775 3500
Connection ~ 3425 3500
Wire Wire Line
	3425 3300 3425 3500
Wire Wire Line
	3425 3700 3775 3700
$Comp
L Device:R_Small R?
U 1 1 5F6D9585
P 4175 3900
AR Path="/5F6D9585" Ref="R?"  Part="1" 
AR Path="/5F687A24/5F6D9585" Ref="R41"  Part="1" 
F 0 "R41" H 4234 3946 50  0000 L CNN
F 1 "9.1K" H 4234 3855 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 4175 3900 50  0001 C CNN
F 3 "~" H 4175 3900 50  0001 C CNN
	1    4175 3900
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R?
U 1 1 5F6D958B
P 4475 3900
AR Path="/5F6D958B" Ref="R?"  Part="1" 
AR Path="/5F687A24/5F6D958B" Ref="R42"  Part="1" 
F 0 "R42" H 4534 3946 50  0000 L CNN
F 1 "24K" H 4534 3855 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 4475 3900 50  0001 C CNN
F 3 "~" H 4475 3900 50  0001 C CNN
	1    4475 3900
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F6D9591
P 4175 4000
AR Path="/5F6D9591" Ref="#PWR?"  Part="1" 
AR Path="/5F687A24/5F6D9591" Ref="#PWR063"  Part="1" 
F 0 "#PWR063" H 4175 3750 50  0001 C CNN
F 1 "GND" H 4180 3827 50  0000 C CNN
F 2 "" H 4175 4000 50  0001 C CNN
F 3 "" H 4175 4000 50  0001 C CNN
	1    4175 4000
	1    0    0    -1  
$EndComp
Wire Wire Line
	4075 3300 4175 3300
Wire Wire Line
	4175 3300 4175 3750
Wire Wire Line
	4175 3750 4475 3750
Wire Wire Line
	4475 3750 4475 3800
Connection ~ 4175 3750
Wire Wire Line
	4175 3750 4175 3800
$Comp
L Device:D_Schottky D?
U 1 1 5F6D959D
P 4725 3150
AR Path="/5F6D959D" Ref="D?"  Part="1" 
AR Path="/5F687A24/5F6D959D" Ref="D1"  Part="1" 
F 0 "D1" V 4679 3229 50  0000 L CNN
F 1 "DSK14" V 4770 3229 50  0000 L CNN
F 2 "Diode_SMD:D_SOD-123" H 4725 3150 50  0001 C CNN
F 3 "~" H 4725 3150 50  0001 C CNN
	1    4725 3150
	0    1    1    0   
$EndComp
$Comp
L Device:L L?
U 1 1 5F6D95A3
P 5025 3000
AR Path="/5F6D95A3" Ref="L?"  Part="1" 
AR Path="/5F687A24/5F6D95A3" Ref="L3"  Part="1" 
F 0 "L3" V 5215 3000 50  0000 C CNN
F 1 "2.2uH" V 5124 3000 50  0000 C CNN
F 2 "Inductor_SMD:L_Taiyo-Yuden_MD-3030" H 5025 3000 50  0001 C CNN
F 3 "~" H 5025 3000 50  0001 C CNN
	1    5025 3000
	0    -1   -1   0   
$EndComp
$Comp
L Device:CP_Small C?
U 1 1 5F6D95A9
P 4325 3250
AR Path="/5F6D95A9" Ref="C?"  Part="1" 
AR Path="/5F687A24/5F6D95A9" Ref="C18"  Part="1" 
F 0 "C18" H 4413 3296 50  0000 L CNN
F 1 "100uF" H 4413 3205 50  0000 L CNN
F 2 "Capacitor_SMD:CP_Elec_6.3x7.7" H 4325 3250 50  0001 C CNN
F 3 "~" H 4325 3250 50  0001 C CNN
	1    4325 3250
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5F6D95AF
P 5325 3100
AR Path="/5F6D95AF" Ref="C?"  Part="1" 
AR Path="/5F687A24/5F6D95AF" Ref="C19"  Part="1" 
F 0 "C19" H 5417 3146 50  0000 L CNN
F 1 "10nF" H 5417 3055 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 5325 3100 50  0001 C CNN
F 3 "~" H 5325 3100 50  0001 C CNN
	1    5325 3100
	1    0    0    -1  
$EndComp
$Comp
L Device:CP_Small C?
U 1 1 5F6D95B5
P 5675 3100
AR Path="/5F6D95B5" Ref="C?"  Part="1" 
AR Path="/5F687A24/5F6D95B5" Ref="C20"  Part="1" 
F 0 "C20" H 5763 3146 50  0000 L CNN
F 1 "470uF" H 5763 3055 50  0000 L CNN
F 2 "Capacitor_THT:C_Radial_D6.3mm_H7.0mm_P2.50mm" H 5675 3100 50  0001 C CNN
F 3 "~" H 5675 3100 50  0001 C CNN
	1    5675 3100
	1    0    0    -1  
$EndComp
Wire Wire Line
	4075 3000 4175 3000
Wire Wire Line
	4725 3000 4875 3000
Connection ~ 4725 3000
Wire Wire Line
	5175 3000 5225 3000
Wire Wire Line
	5325 3000 5675 3000
Connection ~ 5325 3000
Wire Wire Line
	4075 3150 4325 3150
Wire Wire Line
	3775 3700 5225 3700
Wire Wire Line
	5225 3700 5225 3000
Connection ~ 3775 3700
Connection ~ 5225 3000
Wire Wire Line
	5225 3000 5325 3000
$Comp
L power:GND #PWR?
U 1 1 5F6D95C7
P 4325 3350
AR Path="/5F6D95C7" Ref="#PWR?"  Part="1" 
AR Path="/5F687A24/5F6D95C7" Ref="#PWR064"  Part="1" 
F 0 "#PWR064" H 4325 3100 50  0001 C CNN
F 1 "GND" H 4330 3177 50  0000 C CNN
F 2 "" H 4325 3350 50  0001 C CNN
F 3 "" H 4325 3350 50  0001 C CNN
	1    4325 3350
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F6D95CD
P 4725 3300
AR Path="/5F6D95CD" Ref="#PWR?"  Part="1" 
AR Path="/5F687A24/5F6D95CD" Ref="#PWR066"  Part="1" 
F 0 "#PWR066" H 4725 3050 50  0001 C CNN
F 1 "GND" H 4730 3127 50  0000 C CNN
F 2 "" H 4725 3300 50  0001 C CNN
F 3 "" H 4725 3300 50  0001 C CNN
	1    4725 3300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F6D95D3
P 5325 3200
AR Path="/5F6D95D3" Ref="#PWR?"  Part="1" 
AR Path="/5F687A24/5F6D95D3" Ref="#PWR067"  Part="1" 
F 0 "#PWR067" H 5325 2950 50  0001 C CNN
F 1 "GND" H 5330 3027 50  0000 C CNN
F 2 "" H 5325 3200 50  0001 C CNN
F 3 "" H 5325 3200 50  0001 C CNN
	1    5325 3200
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F6D95D9
P 5675 3200
AR Path="/5F6D95D9" Ref="#PWR?"  Part="1" 
AR Path="/5F687A24/5F6D95D9" Ref="#PWR068"  Part="1" 
F 0 "#PWR068" H 5675 2950 50  0001 C CNN
F 1 "GND" H 5680 3027 50  0000 C CNN
F 2 "" H 5675 3200 50  0001 C CNN
F 3 "" H 5675 3200 50  0001 C CNN
	1    5675 3200
	1    0    0    -1  
$EndComp
Wire Wire Line
	3425 3000 3225 3000
Wire Wire Line
	3225 3000 3225 2850
Wire Wire Line
	3225 2650 3225 2600
Wire Wire Line
	3225 2600 4175 2600
Wire Wire Line
	4175 2600 4175 3000
Connection ~ 4175 3000
Wire Wire Line
	4175 3000 4725 3000
$Comp
L power:+12V #PWR?
U 1 1 5F6D95EE
P 4475 4000
AR Path="/5F6D95EE" Ref="#PWR?"  Part="1" 
AR Path="/5F687A24/5F6D95EE" Ref="#PWR065"  Part="1" 
F 0 "#PWR065" H 4475 3850 50  0001 C CNN
F 1 "+12V" H 4490 4173 50  0000 C CNN
F 2 "" H 4475 4000 50  0001 C CNN
F 3 "" H 4475 4000 50  0001 C CNN
	1    4475 4000
	-1   0    0    1   
$EndComp
Wire Wire Line
	5675 2650 5675 3000
Connection ~ 5675 3000
$Comp
L power:GND #PWR?
U 1 1 5F6D95FC
P 3225 3200
AR Path="/5F6D95FC" Ref="#PWR?"  Part="1" 
AR Path="/5F687A24/5F6D95FC" Ref="#PWR062"  Part="1" 
F 0 "#PWR062" H 3225 2950 50  0001 C CNN
F 1 "GND" H 3230 3027 50  0000 C CNN
F 2 "" H 3225 3200 50  0001 C CNN
F 3 "" H 3225 3200 50  0001 C CNN
	1    3225 3200
	1    0    0    -1  
$EndComp
Wire Wire Line
	3425 3150 3225 3150
Wire Wire Line
	3225 3150 3225 3200
Text HLabel 5675 2650 1    50   Input ~ 0
3.3V
Connection ~ 4325 3150
Wire Wire Line
	4325 3150 4325 2650
Text HLabel 4325 2650 1    50   Input ~ 0
12V
Text HLabel 5875 3825 2    50   Input ~ 0
GND
Wire Wire Line
	5675 3200 5800 3200
Wire Wire Line
	5800 3200 5800 3825
Wire Wire Line
	5800 3825 5875 3825
Connection ~ 5675 3200
$EndSCHEMATC
